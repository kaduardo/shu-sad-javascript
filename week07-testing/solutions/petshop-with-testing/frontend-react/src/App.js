import './App.css';
import AnimalFunction from './AnimalFunction';
import AnimalsClass from './AnimalsClass';

function App() {
  return (
    <div className="App">
      <AnimalFunction />
      
      <AnimalsClass />
    </div>
  );
}

export default App;
