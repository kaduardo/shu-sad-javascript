import { createWebHistory, createRouter } from 'vue-router';
 
const routes = [
    {
        path: "/",
        alias: "/animals",
        name: "animals",
        component: () => import("./components/AnimalsList")
    },
    {
        path: "/animals/:id",
        name: "animal-details",
        component: () => import("./components/AnimalDetails")
    },
    {
        path: '/add-animal',
        name: 'add-animal',
        component: () => import("./components/AnimalAdd")
    },
    {
        path: '/users',
        name: 'users',
        component: () => import("./components/UserList")
    },
    {
        path: '/users/:id',
        name: 'user-details',
        component: () => import("./components/UserDetails")
    },
    {
        path: '/add-user',
        name: 'user-add',
        component: () => import("./components/UserAdd")
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;