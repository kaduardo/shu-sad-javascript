import { createWebHistory, createRouter } from 'vue-router';
 
const routes = [
    {
        path: "/",
        alias: "/animals",
        name: "animals",
        component: () => import("./components/AnimalsList")
    },
    {
        path: "/animals/:id",
        name: "animal-details",
        component: () => import("./components/AnimalDetails")
    },
    {
        path: '/add-animal',
        name: 'add-animal',
        component: () => import("./components/AnimalAdd")
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;