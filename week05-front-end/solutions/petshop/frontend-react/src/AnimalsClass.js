import React, { Component } from 'react';
import AnimalFunction from './AnimalFunction';

class AnimalsClass extends Component {
    constructor(props) {
        super(props);
        this.state = {
                animals: [],
                error: null
        };
    } 

    render() {
            let data = this.state.animals || {};
            return (
                <div>
                    <p>Here are the animals</p>
                    <ul>
                        {data}
                    </ul>
            </div>
        );
    }

    componentDidMount() {
        // let animals = [
        //     { id:"1", name:"Logan", age:"5", breed:"Border Collie", colour:"Black, white, tan" }, 
        //     { id:"2", name:"Ralph", age:"2", breed:"Cocker Spaniel", colour:"Ginger" },
        //     { id:"3", name:"Gryphon", age:"9", breed:"Staffie X", colour:"grey" }
        // ];
        // let temp = [];

        // animals.forEach(item => {
        //     temp.push(<AnimalFunction
        //         key={item.id}
        //         name={item.name}
        //         age={item.age}
        //         breed={item.breed}
        //         colour={item.colour}
        //         />);
        // });
        fetch('/petshop/animals')
            .then(data => data.json())
            .then(res => {
                if (res.error)
                    this.setState({ error: res.message });

                let temp = [];
                res.forEach(item => {
                    temp.push(
                        <AnimalFunction
                            key = {item._id}
                            name = {item.name}
                            age = {item.age}
                            breed = {item.breed}
                            colour = {item.colour}
                        />
                    );
                }
            );
        this.setState( {animals: temp} );
        });
    }
}

export default AnimalsClass;