import React from 'react';

const AnimalFunction = (props) => {
   return ( <li>The next animal is {props.name}</li> 
   );
}

export default AnimalFunction;