# SHU-SAD Exercises repo

A repository for saving all the exercises for Software Architecture and Design module.

## Table of contents

- [Week 1. Re-Introduction to JavaScript](./week01-re-intro-js/)
- [Week 3. NodeJs and Express with Mongoose](./week03-nodejs-express-mongoose/)
- [Week 5. Front-end](./week05-front-end/)

## Generating PDFs

The pdf file for each exercise can be created using [pandoc](https://pandoc.org/).

Assuming pandoc is installed in your system, the following command will generate a pdf file.

```bash
pandoc readme.md -f markdown -s -o exercises02.pdf
```

Where:

- `readme.md` is the name of the source file.
- replace the target file extension with docx to generate a word document.
