var express = require('express');
var router = express.Router();
 
// GET at the root just to demonstrate
router.get('/', function(req, res, next) {
   res.send('got a GET request at /');
});

// A local list of animals
var animals = [{ name:'Logan', species:'Dog', breed:'Border Collie', age:'5', colour:'Black with white, blue and tan head' }, {name:'Archie', species:'Dog', breed:'Mongrel', age:'15.5', colour:'White' }]

// GET list of animals to show that we're up and running
router.get('/animals', function(req, res, next) {
  res.render('animals',
      {title: 'Pet store',
       message: 'Hello World! from the <tt>animals</tt> router',
       animals: animals }
  );
});
 
// accept POST request and add a new animal to the db
//router.post('/animals', upload.array(), function (req, res) {
//    res.send('Got a POST request at /animals');
//});
 
// accept PUT request at /animals
router.put('/animals', function (req, res) {
 res.send('Got a PUT request at /animals');
});
 
 
// accept DELETE request at /animals
router.delete('/animals', function (req, res) {
 res.send('Got a DELETE request at /animals');
});
 
module.exports = router;