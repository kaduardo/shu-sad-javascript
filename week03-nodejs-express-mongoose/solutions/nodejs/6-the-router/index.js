var server = require("./server");
var router = require("./router");

//Injecting router into server.
server.start(router.route);
